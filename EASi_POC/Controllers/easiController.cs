﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EASi_POC.Models;
using Microsoft.Office.Core;
using Microsoft.Office.Interop;
using Account = Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook;
using OutlookApp = Microsoft.Office.Interop.Outlook.Application;

namespace EASi_POC.Controllers
{
    public class easiController : Controller
    {

        List<easitable> eTable = new List<easitable>();

        private static List<easitable> TestData() {
            using (testdatabaseEntities testEntity = new testdatabaseEntities())
            {
                return testEntity.easitables.ToList();
            }
        }

        // GET: easi
        public ActionResult Index()
        {
            return View(TestData());
        }

        // GET: easi/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        // GET: easi/Create
        public ActionResult Create()
        {
            {
                var dropDownData = new SelectList(TestData(), "DropField", "DropField");
                ViewData["testdatabase"] = dropDownData;
            }
            return View();
        }

        // POST: easi/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var db = new testdatabaseEntities();
            var formModel = new easitable();
            formModel.Textfield = collection["Textfield"];
            formModel.DropField = collection["DropField"];
            formModel.Label = collection["Label"];

            db.easitables.Add(formModel);
            db.SaveChanges();

            string subjectEmail = "DATABASE UPDATED";
            string bodyEmail = $"The database has been updated with {formModel.Textfield}, {formModel.Label} and {formModel.DropField}";
            OutlookApp outlookApp = new OutlookApp();
            MailItem mailItem = outlookApp.CreateItem(OlItemType.olMailItem);

            var emailAddress = outlookApp.ActiveExplorer().Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
            mailItem.Subject = subjectEmail;
            mailItem.To = emailAddress;
            mailItem.Body = bodyEmail;
            mailItem.Importance = OlImportance.olImportanceNormal;
            mailItem.Send();
            return RedirectToAction("Index");
        }


        // GET: easi/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: easi/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: easi/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: easi/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
